package com.control.back.halo.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.control.back.halo.basic.controller.BaseController;
import com.control.back.halo.basic.entity.vo.TreeNode;
import com.control.back.halo.basic.json.Result;
import com.control.back.halo.manage.entity.Function;
import com.control.back.halo.manage.service.IFunctionService;

@Controller
@RequestMapping("/function")
public class FunctionController extends BaseController {

    @Autowired
    private IFunctionService functionService;

    @RequestMapping({ "", "index" })
    public String index(Model model) {
        model.addAttribute("functions", functionService.findAll());
        return "function/index";
    }

    @RequestMapping("/loadTreeNode")
    @ResponseBody
    public Result loadTreeNode() {
        List<TreeNode> trArray = functionService.loadTree();
        return new Result(true, "查询成功", trArray);
    }

    @RequestMapping("/add")
    public String addFunction(Function function, Model model) {
        functionService.save(function);
        return "redirect:index.html";
    }

    @RequestMapping("/remove")
    public String remove(Long id, Model model) {
        functionService.delete(id);
        return "redirect:index.html";
    }

    @RequestMapping("/findAll")
    @ResponseBody
    public Result findUserFunctions() {
        return new Result(true, "查询成功", functionService.findAll());
    }
}
